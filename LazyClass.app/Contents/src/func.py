import re, os, sys
lib = '/'.join(__file__.split('/')[:-1])+'/lib'
sys.path.insert(0,lib)

from HTMLParser import HTMLParser
import requests as rq
import easywebdav
from bs4 import BeautifulSoup
import json
#import logging

newclass='https://newclasses.nyu.edu'
unescape = HTMLParser().unescape
from urllib import unquote

class Error(Exception):
    def __init__(self, reason):
        self.reason = reason
    def __str__(self):
        return repr(self.reason)

def save(name, content):
    with open(name+'.html', 'w') as f:
        f.write(content)

def test_error( resp, value ):
    if resp.status_code != 200:
        save(resp.request.url, resp.content)
        raise Error(value)

def construct_session(info):
    s = rq.Session()
    s.headers.update({"User-Agent":
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"})

    login_resp = s.get(newclass)
    s.headers.update( {'Referer':login_resp.url} )
    assert "JSESSIONID" in s.cookies.keys()
    assert "BIGipServershibboleth-http" in s.cookies.keys()

    suffix = re.search('<form action="(.+?)" method="post" name="login" id="login">', login_resp.content).group(1)
    base = re.search('(https:\/\/.+?)\/', login_resp.url).group(1)
    redirct_url = base + suffix

    ID = {  'j_username': info['id'],
            'j_password': info['pwd'],
            '_eventId_proceed' : 'Login'}

    redirct_resp = s.post(redirct_url, data=ID)
    test_error(redirct_resp, 'Login Failure')

    home_url = re.search('<form action="(.+?)"', redirct_resp.content).group(1)
    home_url = unescape(home_url)
    values = dict(re.findall('<input type="hidden" name="(.+?)" value="(.+?)"', redirct_resp.content))
    values = { k: unescape(v) for k, v in values.items() }

    home_resp = s.post(home_url, data=values)
    return s

def get_class_link( session ):
    resp = session.get(newclass)
    patten = '<li class="nav-menu"><a href="(.+?)" title="(.+?)"'
    result = re.findall(patten, resp.content)
    result = {i[1]:i[0] for i in result}
    return result

def get_class_tabs( session, class_link ):
    html = session.get(class_link)
    tabs = re.findall('href="(.+?)" title=.+>\n.+class="menuTitle">(.+)<\/span>', html.content)
    tabs = { t[1]:t[0] for t in tabs }
    return tabs

def fetch_assignment( session, assign_link, course_path, chunk_size=1024*1024  ):
    assign_resp = session.get(assign_link)
    iframe_link = re.search('<iframe\t[\w\W]+?src="(.+?)">', assign_resp.content).group(1)
    iframe_resp = session.get(iframe_link)
    soup = BeautifulSoup(iframe_resp.content, 'html.parser')

    assign_path = os.path.join( course_path, 'Assignments' )
    if not os.path.exists(assign_path):
        os.makedirs(assign_path)

    table = soup.table
    if table is None:
        with open( os.path.join(assign_path, 'HW_List.js'), 'w' ) as f:
            f.write('OMG, No HW!!\n')
            f.write('OMG, No HW!!\n')
            f.write('OMG, No HW!!\n')
        return

    hws =  soup.table.find_all('tr')[1:]
    hw_list = []
    for h in hws:
        info = {}

        link = h.find('td', attrs={'headers':'title'})
        link = link.a.attrs['href']
        info['href'] = link
        resp = session.get(link)
        soup = BeautifulSoup(resp.content, 'html.parser')

        summary = soup.find('table', attrs={'class':'itemSummary'})
        for _ in summary.find_all('tr'):
            info[_.th.text.strip(' \n\t')] =  _.td.text.strip(' \n\t')

        attach = soup.find('ul', attrs={'class':'attachList'})
        info['Attachment'] = attach.a.attrs['href'] if attach else None
        ins = soup.find('div',attrs={'class':"textPanel"})
        info['Instruction'] = ins.text if ins else None

        hw_list.append(info)

        if info['Attachment'] is not None:
            hw_name = info['Attachment'].split('/')[-1]
            resp = session.get(info['Attachment'], stream=True)
            try:
                with open( os.path.join( assign_path, hw_name).replace('%20', ' ') , 'wb') as fd:
                    for chunk in resp.iter_content(chunk_size):
                        fd.write(chunk)
            except Exception as e:
                info['Attachment'] = 'Fail_to_Download:'+info['Attachment']

    with open( os.path.join(assign_path, 'HW_List.js'), 'w' ) as f:
        json.dump( hw_list, f, indent=4 )

parse_path = lambda x: '/'.join( x.split('/')[3:] )

def traverse_folder( webdav, path, folders ):
    files = webdav.ls(path)
    d = {}
    for f in files[1:]:
        if f.contenttype == '':
            folders = traverse_folder( webdav, parse_path(f.name), folders )
        else:
            d[ parse_path(f.name) ] = f #key is the path without perfix, f.name with perfix

    folders[path] = d
    return folders

def download_resource( webdav, path, course_path, folders ):
    """ path is the absolute path without the 'dav...' perfix """
    path = path.strip('/')

    if not os.path.exists(course_path):
        os.mkdir( course_path )

    failure = []
    for fld_name in folders:
        local_folder_name = unquote(os.path.join( course_path, fld_name ))
        if not os.path.exists(local_folder_name):
            os.makedirs(local_folder_name)

        for fname in folders[fld_name]:
            loc_name = unquote(os.path.join(course_path, fname))
            try:
                webdav.download(fname, loc_name)
            except Exception, e:
                failure.append('\n\nFail to Download: '+fname)
                failure.append(str(e))

    fail_log = os.path.join(course_path, 'Failed_Download_File.txt')
    if len(failure) > 0:
        with open(  fail_log , 'w' ) as f:
            f.writelines(failure)
    else:
        if os.path.exists(fail_log):
            os.remove(fail_log)

def sync_for_one_class( course_path, address, info ):
    '''update the folder of class_name'''
    webdav = easywebdav.connect('newclasses.nyu.edu', protocol='https', port='443',
                            username=info['id'] ,password=info['pwd'],  path=address)
    folders = traverse_folder( webdav, '.', {})

    if not os.path.exists(course_path):
        os.makedirs(course_path)
    record = os.path.join( course_path, '.file_tree.js' )

    his_folders = None
    if os.path.exists( record ):
        his_folders = json.load(open(record,'r'))
    json.dump( folders, open( record, 'w'), indent=4 )

    if his_folders:
        for fname, files in folders.items():
            if fname not in his_folders:
                continue   # if folder not exits before, continue

            hfiles = his_folders[fname]
            fkeys = files.keys()
            for f in fkeys:
                local_name = unquote(os.path.join( course_path, f ))
                if f not in hfiles:
                    continue
                if list(files[f]) == hfiles[f] and os.path.exists( local_name ):
                    files.pop(f)

            if len(files) == 0:
                folders.pop(fname)
    # print folders
    download_resource( webdav, '.', course_path, folders)
