import func as F
from func import Error
import json
import os, sys
from os.path import join

if sys.argv[-1] == 'run':
    test_mode = False    
else:
    import logging, traceback
    test_mode = True
    if sys.argv[-1] == 'test':
        logging.basicConfig(level=logging.INFO)
    elif sys.argv[-1] == 'debug':
        logging.basicConfig(level=logging.DEBUG)

try:
    direc = '/'.join(__file__.split('/')[:-1])
    info = join( direc , '.no_peek')
    if not os.path.exists(info):
        info = join( os.environ['HOME'], 'Documents', '.no_peek')
        if not os.path.exists( info ):
            raise Error('Cannot find correct information about NetID, Password or Destination Folder\n \
                            Please setup again.')

    with open(info, 'r') as f:
        info = json.load(f)

    if len(info) == 0 or info is None or not isinstance(info, dict):
        raise Error('Cannot find correct information about NetID, Password or Destination Folder\n Please setup again.')

    session = F.construct_session(info)
    class_dict = F.get_class_link(session)

    for c, link in class_dict.items():
        tabs = F.get_class_tabs(session,  link )
        c = c.split(',')[0]
        course_path = join(info['folder'], c)
        dav_link = 'dav/'+link.split('/')[-1].strip('/') + '/'
        F.sync_for_one_class( course_path, dav_link, info )
        F.fetch_assignment(session, tabs['Assignments'], course_path )

    print 'Sync Done. Enjoy your files!'
except Exception as e:
    if test_mode:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=sys.stdout)
    else:
        print e
